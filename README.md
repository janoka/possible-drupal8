# Installation

The solution is based on [Docker-based Drupal stack by Wodby](https://github.com/wodby/docker4drupal) project.

(The solution was tested on MacOS.)

## Prerequisite

### Docker

For Docker installation please see [this page](https://docs.docker.com/install/overview/).

## Getting Started

Please run all of the following commands in the repository root.

1. Build the environment
    ```shell
    make build
    ```
    Or:
    ```shell
    docker-compose pull
    docker-compose up -d
    docker exec possible-drupal8_php composer install
    ```
2. Open the main [page](http://drupal.docker.localhost:8000).
3. Please use the following credential to [login](http://drupal.docker.localhost:8000/en/user/login):

   Username: **admin**
   Password: **pass**

Database automatically import during the start.

## Sites Requirements

Create a multi-site, multilingual portal with a main page. 

The main page should contain the following:

- Changeable Logo

  You can change the logo [here](http://drupal.docker.localhost:8000/en/admin/appearance/settings) by changing _logo image_.
  
- Site and language switcher

  Please see the [main page](http://drupal.docker.localhost:8000).
  
- Navigation menu on the top leading to simple content pages

  Please also see the [main page](http://drupal.docker.localhost:8000).

- Editable slider with images, texts, links

  You can use the [Slider content type](http://drupal.docker.localhost:8000/en/admin/content?title=&type=slider&status=All&langcode=All) and see the result on the [main page](http://drupal.docker.localhost:8000).

- 5 latest news with social share buttons (Facebook Twitter, G+)

  Please also see the [main page](http://drupal.docker.localhost:8000).
  When you click one of the items, you will be able to see social share icons. ([Example](http://drupal.docker.localhost:8000/en/vivamus))

- One editable custom video from YouTube, opening in modal dialog window

  Please [see](http://drupal.docker.localhost:8000/en/vivamus).

- Editable footer

  When you logged in, then you will be able to edit _"Our Location"_ at the _Footer_. You can use the contextual menu to do that.
  Or just follow this [link](http://drupal.docker.localhost:8000/en/block/5).

Other requirements:

- The site should support at least 2 sites with 2-2 languages (the navigation menu elements should be different in each version)

  Ok.  

- The site should have responsive layout

  Ok.

- The hand over package must contain both the code files and database dump

  Ok. Database automatically following the start.
  
  For any case please see the dump file in the `mariadb/init/possible-20181024-1530.mysql.gz` file. 
  
- The solution should contain a read-me / installation document

  Ok.

- Composer should be used to use contrib modules

  Ok.

- Optionally optimize Lighthouse/pagespeed scores of the page

- Optionally use dockerfile / docker-compose

  Ok.

## Site Links

- [Main Site](http://drupal.docker.localhost:8000)
- [Site 1.](http://site1.docker.localhost:8000)
- [Site 2.](http://site2.docker.localhost:8000)

## Resources

- [Repository of Docker-based Drupal stack by Wodby](https://github.com/wodby/docker4drupal)
- [Documentation of Docker-based Drupal stack by Wodby](https://wodby.com/docs/stacks/)
- [Drupal Europe 2018 - "Driesnote" (by Dries Buytaert)](https://www.youtube.com/watch?v=iXB0sNreSlM)
- [Photo by Lucas Benjamin](https://unsplash.com/photos/vEs6pUtpALo)
- [Photo by Markus Henze](https://unsplash.com/photos/Gc0RaGatXJE)
- [Photo by Chloé Mg](https://unsplash.com/photos/YUGwPTzIg58)
