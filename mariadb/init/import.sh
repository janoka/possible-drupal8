#!/usr/bin/env bash

for f in /docker-entrypoint-initdb.d/*.mysql.gz; do
  /usr/bin/gunzip -c "$f" | /usr/bin/mysql --user="$MYSQL_USER" --password="$MYSQL_PASSWORD" "$MYSQL_DATABASE"
done
